package formatting

import (
	"strings"
	"unicode"
)

type ReplaceChars struct {
	Opening string
	Closing string
}

var HTMLReplaces = map[string]ReplaceChars{
	"*":   {"<b>", "</b>"},
	"_":   {"<i>", "</i>"},
	"~":   {"<s>", "</s>"},
	"```": {"<code>", "</code>"},
}

var DefaultReplaces = map[string]ReplaceChars{
	"*":   {"**", "**"},
	"_":   {"__", "__"},
	"~":   {"~~", "~~"},
	"```": {"```", "```"},
}

func Replace(waText string, replaces map[string]ReplaceChars) string {
	var output = waText
	for find, replace := range replaces {
		output = ReplaceChar(output, find, replace)
	}

	return output
}

func ReplaceChar(text string, find string, replace ReplaceChars) string {
	var output string
	opening := FindNextOpening(text, find)
	for opening != -1 {
		closing := FindNextClosing(text[opening+len(find):], find)
		if closing != -1 {
			if opening > 0 {
				output += text[:opening] + replace.Opening + text[opening+len(find):closing+opening+len(find)] + replace.Closing
			} else {
				output += replace.Opening + text[opening+len(find):closing+opening+len(find)] + replace.Closing
			}
			text = text[opening+len(find)+closing+len(find):]
		} else {
			break
		}
		opening = FindNextOpening(text, find)
	}
	output += text

	return output
}

func FindNextOpening(text string, find string) int {
	index := strings.Index(text, find)
	if ((index + len(find)) < len(text)) && (index >= 0) {
		if !unicode.IsSpace([]rune(text)[index+len(find)]) {
			return index
		}
		nextIndex := FindNextOpening(text[index+len(find):], find)
		if nextIndex != -1 {
			return (index + len(find)) + nextIndex
		}
	}
	return -1
}

func FindNextClosing(text string, find string) int {
	index := strings.Index(text, find)
	if index >= 0 {
		if index == 0 {
			return index
		} else if !unicode.IsSpace([]rune(text)[index-1]) {
			return index
		} else {
			nextIndex := FindNextClosing(text[index+len(find):], find)
			if nextIndex != -1 {
				return (index + len(find)) + nextIndex
			}
		}
	}
	return -1
}
