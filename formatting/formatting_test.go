package formatting

import (
	"testing"
)

func TestReplace(t *testing.T) {

	tables := []struct {
		text     string
		formated string
	}{
		{"*Test*", "<b>Test</b>"},
		{"_Test_", "<i>Test</i>"},
		{"~Test~", "<s>Test</s>"},
		{"```Test```", "<code>Test</code>"},
		{"*Das _ist ~ein~_ Test*", "<b>Das <i>ist <s>ein</s></i> Test</b>"},
		{"*_~```Test```~_*", "<b><i><s><code>Test</code></s></i></b>"},
		{"*Test*, hallo *Test*", "<b>Test</b>, hallo <b>Test</b>"},
		{"*Test", "*Test"},
		{"* Test*", "* Test*"},
		{"*Test *", "*Test *"},
		{"Test*", "Test*"},
		{`Hallo sehr tolle Menschen 🥰💚
		Wir haben diese Woche zwei super Webinare der internen #WebinareforFuture Reihe für euch 🥳
		
		*Montag Webinar, 11.05., 20 Uhr*
		Einführung in *BigBlueButton - ein neues Konferenztool*
		BBB ist ein Open Source Tool das als Alternative zu Zoom mit Datenschutz und kostenloser Verwendung punkten kann 🌟
		Die Website AG hosted jetzt diese Alternative für Webinare auf dem FFF Server, die sie uns hier vorstellt! 🇮🇸
		https://us02web.zoom.us/j/81140339138
		
		*Donnerstag, 14.05., 19 Uhr*
		Onboarding der Workshop AG/ Webinare for Future - Orga
		Für alle die Lust haben sich an der Organisierung von unseren Webinaren beteiligen wollen :)
		Mehr Infos: https://pad.fridaysforfuture.is/p/callforpartyWS
		https://us02web.zoom.us/j/5445674464
		
		*Freitag Webinar, 15.05., 19 Uhr*
		Sharepics erstellen mit unserem tollen *Toolpic*!  💫
		https://us02web.zoom.us/j/89437717160
		
		*Samstag, 16.05. , 15 Uhr*
		Onboarding der Workshop AG/ Webinare for Future- Orga
		s.O.
		https://us02web.zoom.us/j/5445674464
		
		Ihr wollt immer über die *aktuellen Webinare informiert* werden?
		
		WhatsApp: https://fffutu.re/WebinarInfoWA
		
		Telegram: https://fffutu.re/WebinarInfoTG
		
		Ihr wollt *verpasste Webinare* nochmal anschauen?
		https://fffutu.re/AufzeichnungWebinareIntern`,
			`Hallo sehr tolle Menschen 🥰💚
		Wir haben diese Woche zwei super Webinare der internen #WebinareforFuture Reihe für euch 🥳
		
		<b>Montag Webinar, 11.05., 20 Uhr</b>
		Einführung in <b>BigBlueButton - ein neues Konferenztool</b>
		BBB ist ein Open Source Tool das als Alternative zu Zoom mit Datenschutz und kostenloser Verwendung punkten kann 🌟
		Die Website AG hosted jetzt diese Alternative für Webinare auf dem FFF Server, die sie uns hier vorstellt! 🇮🇸
		https://us02web.zoom.us/j/81140339138
		
		<b>Donnerstag, 14.05., 19 Uhr</b>
		Onboarding der Workshop AG/ Webinare for Future - Orga
		Für alle die Lust haben sich an der Organisierung von unseren Webinaren beteiligen wollen :)
		Mehr Infos: https://pad.fridaysforfuture.is/p/callforpartyWS
		https://us02web.zoom.us/j/5445674464
		
		<b>Freitag Webinar, 15.05., 19 Uhr</b>
		Sharepics erstellen mit unserem tollen <b>Toolpic</b>!  💫
		https://us02web.zoom.us/j/89437717160
		
		<b>Samstag, 16.05. , 15 Uhr</b>
		Onboarding der Workshop AG/ Webinare for Future- Orga
		s.O.
		https://us02web.zoom.us/j/5445674464
		
		Ihr wollt immer über die <b>aktuellen Webinare informiert</b> werden?
		
		WhatsApp: https://fffutu.re/WebinarInfoWA
		
		Telegram: https://fffutu.re/WebinarInfoTG
		
		Ihr wollt <b>verpasste Webinare</b> nochmal anschauen?
		https://fffutu.re/AufzeichnungWebinareIntern`},
	}

	for _, table := range tables {
		defer func() {
			if err := recover(); err != nil {
				t.Errorf("Got panic (at \"%s\" = \"%s\"): %s", table.text, table.formated, err)
			}
		}()
		returnValue := Replace(table.text, HTMLReplaces)
		if returnValue != table.formated {
			t.Errorf("Formating of \"%s\" was incorrect, got: \"%s\", want \"%s\"", table.text, returnValue, table.formated)
		}
	}
}

func TestFindNextOpening(t *testing.T) {
	tables := []struct {
		text         string
		indexOpening int
	}{
		{"*Test*", 0},
		{"Te*st*", 2},
		{"Te* st*er", 6},
	}

	for _, table := range tables {
		returnValue := FindNextOpening(table.text, "*")
		if returnValue != table.indexOpening {
			t.Errorf("Index in '%s' was incorrect, got: %v, want %v", table.text, returnValue, table.indexOpening)
		}
	}
}
