package main

import (
	"log"
	"os"
	"time"

	"gitlab.com/fff-bots/wa2tgbot/handlers"

	tb "gopkg.in/tucnak/telebot.v2"
)

func main() {
	if os.Getenv("WA2TG_TOKEN") == "" {
		log.Fatal("Please provide bot token via WA2TG_TOKEN env var")
		return
	}

	b, err := tb.NewBot(tb.Settings{
		Token:  os.Getenv("WA2TG_TOKEN"),
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	bc := handlers.BotContext{
		Bot: b,
	}

	b.Handle(tb.OnQuery, bc.InlineHandler)
	b.Handle(tb.OnText, bc.PmHandler)

	// Command handlers
	b.Handle("/start", bc.StartHandler)
	b.Handle("/license", bc.LicenseHandler)

	b.Start()
}
