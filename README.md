# About
wa2tgbot is a simple Telegram bot to convert the formatting of messages copied from Whatsapp to the Telegram formatting.

# Usage
Currently there are two types of operation:

1. use the bot in inline mode, you can simply do this be prefixing your message with the bots username (be aware that inline mode just works for messages up to 256 characters)
2. user the bot via direct message, just send the bot a direct message and it will answer back with two types of your text. One of them is plain but with the characters for formatting so you can just copy it to the message box. The other message is already formated so you could simply forward it.

# Installation
```bash
$ go get -u gitlab.com/fff-bots/wa2tgbot
$ go install gitlab.com/fff-bots/wa2tgbot
```

Then you will find the binary in $GOPATH/bin.

# Docker
## without docker-compose
* `sudo docker build . -t wa2tg`
* `sudo docker run -e WA2TG_TOKEN=TOKEN --rm wa2tg`

# Config
Set your bot token via the $WA2TG_TOKEN environment variable.

# Dev-Setup
Clone the source and modify it if you want.
To run and test your changes do the following:
```bash
$ WA2TG_TOKEN="yourtgtokenhere" go run .
```
