module gitlab.com/fff-bots/wa2tgbot

go 1.14

require (
	github.com/pkg/errors v0.9.1 // indirect
	gopkg.in/tucnak/telebot.v2 v2.3.3
)
