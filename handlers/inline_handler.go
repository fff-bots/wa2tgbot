package handlers

import (
	"html"
	"log"

	"gitlab.com/fff-bots/wa2tgbot/formatting"
	tb "gopkg.in/tucnak/telebot.v2"
)

func (bc BotContext) InlineHandler(q *tb.Query) {
	if q.Text == "" {
		return
	}
	results := make(tb.Results, 1)

	title := "Convert the message to Telegram formatting"
	description := "YYour message must be 256 characters or shorter. If it's not send me it via a direct message"
	if q.From.LanguageCode == "de" {
		title = "Nachricht zur Telegram Formatierung konvertieren"
		description = "Die Nachricht darf nicht länger als 256 Zeichen sein, falls sie es sein sollte schicke sie mir bitte via Direktnachricht"
	}
	result := &tb.ArticleResult{
		Title:       title,
		Description: description,
	}
	content := tb.InputTextMessageContent{
		Text:      formatting.Replace(html.EscapeString(q.Text), formatting.HTMLReplaces),
		ParseMode: tb.ModeHTML,
	}
	result.SetContent(&content)
	results[0] = result

	err := bc.Bot.Answer(q, &tb.QueryResponse{
		Results:   results,
		CacheTime: 60,
	})

	if err != nil {
		log.Println(err)
	}
}
