package handlers

import (
	"html"

	"gitlab.com/fff-bots/wa2tgbot/formatting"
	tb "gopkg.in/tucnak/telebot.v2"
)

func (bc BotContext) PmHandler(m *tb.Message) {
	unformattedMessage := "Here is your text with formatting characters for Telegram, copy it into the send box:"
	formattedMessage := "Here is how the text will look like. You could also just forward this already formatted text:"
	if m.Sender.LanguageCode == "de" {
		unformattedMessage = "Hier ist dein Text mit den Formatierungszeichen für Telegram. Du kannst ihn einfach in die Nachrichtenbox kopieren:"
		formattedMessage = "So wird der Text dann aussehen. Du kannst diese schon formatierte Variante natürlich auch einfach weiterleiten:"
	}
	bc.Bot.Send(m.Sender, unformattedMessage)
	bc.Bot.Send(m.Sender, formatting.Replace(m.Text, formatting.DefaultReplaces), &tb.SendOptions{
		ParseMode: tb.ModeDefault,
	})
	bc.Bot.Send(m.Sender, formattedMessage)
	bc.Bot.Send(m.Sender, formatting.Replace(html.EscapeString(m.Text), formatting.HTMLReplaces), &tb.SendOptions{
		ParseMode: tb.ModeHTML,
	})
}
