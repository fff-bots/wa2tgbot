package handlers

import (
	tb "gopkg.in/tucnak/telebot.v2"
)

func (bc BotContext) LicenseHandler(m *tb.Message) {
	text := "Whatsapp2Telegram formatting bot by @jugendhacker\n\n"
	text += "This bot is published under [AGPLv3](https://gitlab.com/fff-bots/wa2tgbot/-/blob/master/LICENSE).\n"
	text += "You can find the sourcecode here: [https://gitlab.com/fff-bots/wa2tgbot/](https://gitlab.com/fff-bots/wa2tgbot/)"
	if m.Sender.LanguageCode == "de" {
		text = "Whatsapp2Telegram Formatierungs bot von @jugendhacker\n\n"
		text += "Dieser Bot wurde unter der [AGPLv3](https://gitlab.com/fff-bots/wa2tgbot/-/blob/master/LICENSE) veröffentlicht.\n"
		text += "Der Quelltext ist unter [https://gitlab.com/fff-bots/wa2tgbot/](https://gitlab.com/fff-bots/wa2tgbot/) zu finden"
	}

	bc.Bot.Send(m.Sender, text, &tb.SendOptions{
		ParseMode: tb.ModeMarkdown,
	})
}
