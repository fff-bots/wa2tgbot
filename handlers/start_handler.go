package handlers

import (
	tb "gopkg.in/tucnak/telebot.v2"
)

func (bc BotContext) StartHandler(m *tb.Message) {
	message := "Hi\n"
	message += "I'm a little helper bot. I want you to help converting the formatting of a message copied from Whatsapp to the Telegram formating.\n\n"
	message += "You could either use me by prefixing your message with @" + bc.Bot.Me.Username + " (this only works up to 256 character long messages).\n"
	message += "Or you could use me by sending me the text you copied from whatsapp directly here."
	if m.Sender.LanguageCode == "de" {
		message = "Hallo\n"
		message += "Ich bin ein kleiner Helfer. Ich helfe dir dabei die Formatierung von Nachrichten die du von Whatsapp kopiert hast auf Telegram anzupassen.\n\n"
		message += "Du kannst mich entweder benutzen indem du @" + bc.Bot.Me.Username + " vor deine Nachricht schreibst (achtung das geht nur mit Nachrichten bis zu 256 Zeichen).\n"
		message += "Oder du schickst mir deine von Whatsapp kopierte Nachricht direkt hier."
	}
	bc.Bot.Send(m.Sender, message)
}
